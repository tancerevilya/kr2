#include <iostream>
#include <cmath>
using namespace std;
double f(double x)
{
    //int a=1;
    //int b=2;
    //x = 21*(a*a)-34*a*b+9*(b*b);
    return sin(x);
}

double left_rect(double a, double b, int n)
{
    double h = (b-a)/n;
    double sum = 0.0;
    for (int i=0; i<=n-1; i++)
    {
        sum += h*f(a+i*h);
    }
    return sum;
}

double right_rect(double a, double b, int n)
{
    double h = (b-a)/n;
    double sum = 0.0;
    for (int i=1; i<=n; i++)
    {
        sum += h*f(a+i*h);
    }
    return sum;
}

double trapeze(double a, double b, int n)
{
    double h = (b-a)/n;
    double sum = f(a)+f(b);
    for (int i=1; i<=n; i++)
    {
        sum += 2*f(a+i*h);
    }
    sum *= h/2;
    return sum;
}

double simpsom(double a, double b, int n)
{
    double h = (b-a)/n;
    double sum = f(a)+f(b);
    int k;
    for (int i=1; i<=n; i++)
    {
        k = 2+2*(i%2);
        sum += k*f(a+i*h);
    }
    sum *= h/3;
    return sum;
}

int main()
{
    double a, b;
    int n;
    a = 1;
    b = 2;
    n = 1000;
    cout<<"left_rect "<< left_rect(a, b, n)<<endl;
    cout<<"right_rect "<< right_rect(a, b, n)<<endl;
    cout<<"trapeze "<< trapeze(a, b, n)<<endl;
    cout<<"simpsom "<< simpsom(a, b, n)<<endl;
    return 0;
}
