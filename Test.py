from math import cosh, log, sin

x = float(1)
a = float(2)
n = int(1000)

def f(x):
    #x = -(10*(18*a**2+11*a*x-24*x**2)/(-a**2+a*x+6*x**2))
    return sin(x)

def left_rect(x, a, n):
    h = float((a-x)/n)
    sum = float(0.0)
    i = int(0)
    for i in range(0, i <= n-1):
        sum += h * f(x+i*h)
        i=i+1
    return sum

def right_rect(x, a, n):
    h = float((a-x)/n)
    sum = float(0.0)
    i = int(1)
    for i in range(1, i >= n):
        sum += h * f(x+i*h)
        i=i+1
    return sum

def trapeze(x, a, n):
    h = float((a-x)/n)
    sum = float(f(x)+f(a))
    i = int(1)
    for i in range(1, i <= n-1):
        sum += 2 * f(x+i*h)
        i=i+1
    sum *= h/2
    return sum

def simpson(x, a, n):
    h = float((a-x)/n)
    sum = float(f(x)+f(a))
    k = int(0)
    i = int(0)
    for i in range(1, i <= n-1):
        k = 2+2*(i%2)
        sum += k * f(x+i*h)
        i=i+1
    sum *= h/3
    return sum

print("Рассчеты для функции G: ")
print("Left rect = ", left_rect(x, a, n))
print("Right rect = ", right_rect(x, a, n))
print("Trapeze = ", trapeze(x, a, n))
print("Simpson = ", simpson(x, a, n))

