from math import cosh, log, sin

a = float(1)
b = float(2)
n = int(1000)

def f(x):
    a = int(1)
    b = int(2)
    x = 21*(a**2)-34*a*b+9*(b**2)
    return cosh(x)

def left_rect(a, b, n):
    h = float((b-a)/n)
    sum = float(0.0)
    i = int(0)
    for i in range(0, i<=n-1):
        sum += h * f(a+i*h)
        i=i+1
    return sum

def right_rect(a, b, n):
    h = float((b-a)/n)
    sum = float(0.0)
    i = int(1)
    for i in range(1, i<=n):
        sum += h * f(a+i*h)
        i=i+1
    return sum

def trapeze(a, b, n):
    h = float((b-a)/n)
    sum = float(f(a)+f(b))
    i = int(1)
    for i in range(1, i <= n-1):
        sum += 2 * f(a+i*h)
        i=i+1
    sum *= h/2
    return sum

def simpson(a, b, n):
    h = float((b-a)/n)
    sum = float(f(a)+f(b))
    k = int(0)
    i = int(0)
    for i in range(1, i <= n-1):
        k = 2+2*(i%2)
        sum += k * f(a+i*h)
        i=i+1
    sum *= h/3
    return sum

print("Рассчеты для функции G: ")
print("Left rect = ", left_rect(a, b, n))
print("Righ rect = ", right_rect(a, b, n))
print("Trapeze = ", trapeze(a, b, n))
print("Simpson = ", simpson(a, b, n))
